\documentclass[12pt]{article}
\usepackage{graphicx}
\usepackage{float}
\usepackage{blindtext}
\usepackage{scrextend}
\addtokomafont{labelinglabel}{\sffamily}
\usepackage[utf8]{inputenc}
\usepackage[toc,page]{appendix}

\renewcommand{\figurename}{Obrázek}
\begin{document}

\title{Projekt RSSP}
\author{Petr Dvořáček}


\maketitle

\begin{abstract}
    Práce se zabývá možnostmi vytvoření businessu v oboru OCR s využitím opensource technologií. Je zde popsáno technologické řešení pro aplikaci, která automaticky čte data z faktur. Aby aplikace byla jednoduše škálovatelná, bylo použito cloudových funkcí\cite{gfunc} pro backend a Firebase\cite{firebase} pro servování frontendu. Toto řešení bylo zvoleno zejména pro jednoduchost implementace a bezpečnost. Pokud bychom zvolili řešení založené na VPS, případně na bare-metal serveru a chtěli bychom zachovat stejnou škálovatelnost, implementace by byla extrémně náročná (instalace Kubernetes, tvorba vlastního Loadbalanceru, zajištění dostupnosti ze všech koutů světa).  
\end{abstract}


\section{Úvod do problematiky}
Automatické zpracování faktur z technologického hlediska představuje velikou výzvu, a to zejména proto, že pro toto neexistuje žádný všeobecně přijatý standart. V této práci se zabývám návrhem softwarového řešení pro extrakci dat z faktury (obr. \ref{basicFlow}). Při návrhu tohoto řešení je diskutován jednak způsob, jakým by samotné čtení bylo možné provést a jednak návrh technologického řešení jako webové služby pro zákazníka.

Inspirací pro tuto práci mi byla bakalářská práce Miloslava Szczypka a aplikace Rossum, což je produkt jednoho z největších českých startupů.\cite{rossum}
\begin{figure}[H]
    \includegraphics[width=\textwidth]{./pics/rsspFlow.pdf}
    \caption{Přiblížení cíle projektu. Faktura ve formě obrázku (vlevo) je transformována na stěžejní data která obsahuje (vpravo).}
    \label{basicFlow}
\end{figure}

\section{Analýza technologického řešení}
V této sekci bude popsáno technologické řešení jak samotné webové aplikace pro prohlížeč, tedy GUI ve kterém si uživatel bude moci \textit{naklikat} požadované akce, tak webové služby, která bude z této aplikace volána.

\subsection{Analýza webové aplikace pro prohlížeč}
Webová aplikace bude napsána v jazyku \textit{React}, jehož syntaxe z velké části vychází z JavaScriptu a React samotný je do JavaScriptu následně \textit{transpilován}\footnote{\textit{Transpilace} znamená překlad jednoho jazyka do jiného, narozdíl od \textit{Kompilace}. Hlavní rozdíl je v tom, že při \textit{Transpilaci} jsou oba jazyky na stejné úrovni abstrakce, kdežto při \textit{kompilaci} je kompilován výše abstraktní jazyk do nižšího (Python do C)}.

Nasazení aplikace bude řešeno přes \textit{Firebase}\cite{firebase}, protože toto řešení umožňuje nasazení aplikace i spolu s jejími cloudovými funkcemi\footnote{Firebase a Google Cloud Platform jsou jeden produkt. Firebase vznikl jako samostatný startup v roce 2011 a byl následně koupen firmou Google v roce 2014. Pro zachování značky (\textit{brandu}) byl ponechán název Firebase, nicméně Firebase běží na Google Cloud Platform infrastruktuře.\cite{fbWiki}} \textit{jedním příkazem}, pro náš účel se tedy skvěle hodí. Webová aplikace bude s použitím služby Firebase servována přes HTTP server jako statická jednostránková aplikace (toto se dá zohnout a používat url cestu pro vykreslování stranky.)

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{pics/appDiagram.pdf}
    \caption{Stavový diagram webové aplikace. Nejedná se o korektní vyobrazení, protože v rozhodovacím znaku je text.}
    \label{fig:diagramApp}
\end{figure}



\subsection{Analýza webové služby}
Samotný algoritmus bude dostupný jako webová služba přes \textit{loadbalancer}\cite{loadBal}. Tento přístup umožňuje v prvé řadě vysokou úroveň zabezpečení zdrojového kódu, dále dobrou škálovatelnost služby. Loadbalancer funguje jako \textit{proxy server} která pouze přeposílá požadavky na další služby, které jsou ukryté za ní (viz obrázek \ref{fig:loadBal}). Škálování aplikace bude provedeno na základě vztížení jednotlivých služeb, load balancer bude sbírat informace o tomto vztížení a dle potřeby bude vytvářet nové služby.\footnote{Tyto nové služby mohou běžet na jiných fyzických serverech. Komunikace stále probíhá přes REST API.}
\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{./prezentace/pics/services.pdf}
    \caption{Schéma síťové struktury služby ukryté za load balancerem.}
    \label{fig:loadBal}
\end{figure}
Vytvoření škálovatelné webové služby, případně celé aplikace je možné s využitím opensource řešení pro konteinerizaci aplikací \textit{Docker} spolu s řešením pro orchestraci aplikací \textit{Kubernetes}\cite{docker,k8s}. V našem případě však využijeme odlišný přítup, a sice \textit{out-of-the-box} řešení v podobě cloudovch funkcí \cite{gfunc}. Postup práce bude následovný: naimplementujeme logiku webové služby (POST, GET, atd. požadavky a logiku algoritmu), zvolíme \textit{endpoint}, tedy url REST API, které budeme volat a tento balíček v podobě funkce nasadíme na Google Cloud Platform. Tato cloudová platforma se postará o škálování a běh webové služby. 

Zvolený přístup je výhodný zejména v jednoduchosti a rychlosti vývoje. Nevýhoda spočívá ve vyšších finančních nákladech na běh aplikace.

\subsection{Analýza technologií pro OCR}

Pro samotnou funkcionalitu OCR bude použita knihovna \textit{Tesseract} \cite{tesseractOverview}, jejíž kód je opensource pod licencí \textit{Apache} \cite{apacheLic}. 

Na obrázku \ref{fig:tesseractBasic} je diagram datových toků při čtení obrázku knihovnou tesseract. API Request přivede obrázek na vstup, tento je následně předzpracován a poslán dále do Tesseract OCR Engine. Tesseract OCR Engine využívá knihovnu \textit{Leptonica} \cite{leptonica}, díky které podporuje Tesseract mnoho formátů obrázku. Do OCR Engine dále vstupuje Trained Data Set, který představuje natrenovanou strukturu znaků. Výstup ze samotného OCR Engine vstupuje do Post-Processoru, kde jsou použity lexografické korekce výstupu, například přiřazení slova které Engine přečetl chybně slovu správnému na základě dostupného slovníku \cite{tesseractBook}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{pics/tesseractDiagram.pdf}
    \caption{Diagram datových toků při čtení textu s pomocí knihovny Tesseract.}
    \label{fig:tesseractBasic}
\end{figure}

Základem pro získání smysluplných dat z obrázku faktury je předvytvoření vzoru pro fakturu. Tento vzor bude definovat pozici daných datových oblastí (například email, telefon, adresa). Před samotným aplikováním OCR je tedy nutno obrázek rozstříhat dle vzoru a týto výstřižky poté poslat do OCR enginu. Tento workflow je na obrázku \ref{fig:appTok}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{prezentace/pics/dataFlow.pdf}
    \caption{Tok dat ve výsledné aplikace}
    \label{fig:appTok}
\end{figure}


\section{Analýza softwarová}
V této sekci je popsáno softwarové řešení pro čtení dat z faktur. 

\paragraph{Analýza struktury vnějšího prostředí} S výsledným produktem bude moci zacházet zákazník jak ve formě webové aplikace, tak ve formě webového REST API.
\paragraph{Analýza funkcí} Software umožňuje přečíst data z faktury ve formátu .jpg, a nebo .png. Přečtená data budou reprezentována ve formě JSON objektu. 
\paragraph{Analýza komunikací} Hlavní řídící algoritmus (v tomto pípadě webová aplikace) bude komunikovat s částí OCR enginu přes HTTP protokol. OCR engine bude jako cloudová funkce\cite{gfunc} vystavovat REST API.
\begin{labeling}{alligator}
    \item [Licenční poplatky] Pro provozovatele je díky tomuto přístupu jednoduché spravovat licenční poplatky -- tyto se mohou odvíjet od počtu přečtených faktur.
    \item [Zabezpečení] Uživatel nemá přístup k jádru aplikace (OCR engine). Engine je ukryt v cloudové infrastruktuře poskytovatele. Pokud by kdokoliv chtěl hacknout aplikaci a získat engine pro čtení faktur, musel by nejdříve hacknout cloudovou infrastrukturu (v případě správné konfigurace aplikace).
    \item [Jednoduchost implementace] Po službě chceme jedinou věc -- aby přijala obrázek, tento zpracovala, přečetla a vrátila přečtený text. Pro toto není třeba implementovat celý webový server. Řešení přes cloudovou funkci je dobře škálovatelné a jednodušší na lazení.\footnote{Slovo \textit{jednodušší} je v akademické sféře velmi ošemetné. V tomto kontextu je myšleno v sovislosti s omezením problému. Pokud problém vytvoření webového serveru zjednodušíme pouze na problém volání funkce, je lazení výsledné služby méně komplikované, přímější a elegantnější -- \textit{jednodušší}.}
\end{labeling}
\paragraph{Analýza slabých míst} Dopředu víme, že aplikace může selhat v případě nahrání příliš poškozeného obrázku, který by nebyl schopen přečíst ani člověk. Dále aplikace nebude plně funkční v případě, že nebude k dispozici dostatečně stabilní internetové připojení (umožňující přenést obrázek). Problém může také nastat v případě že uživatel bude aplikaci používat v jiném než podporovaném prohlížeči.

\subsection{Softwarové řešení aplikace}

\subsection{Softwarové řešení služby}
Webová služba pro OCR představje klíčovou funkcionalitu aplikace. V této sekci bude popsána její implementace. 







\section{Systémová specifikace}
\paragraph{Výchozí situace a cíle} Cílem aplikace je nabídnout zákazníkovi možnost zautomatizování některých procedur v účetnictví a vedení firmy. Aplikace by měla zákazníkovi umožnit nahrát obrázek faktury, tuto přečíst a vrátit přečtená data. Tohoto bude docíleno webovou aplikací, ale i poskytnutím přístupu na REST API webové služby.
\paragraph{Vztah okolí k provozování systému} Pro správný chod aplikace je třeba zajistit, že zákazník aplikaci používá na počítači se \textbf{stabilním internetovým připojením} a v prohlížeči \textit{Firefox Standard Release} \textbf{}{verze 71.0} a vyšší.
\paragraph{Funkční požadavky} Od technologie očekáváme, že bude umožňovat vývoj webových služeb ve formě cloud funkce.
\paragraph{Nefunkční (ostatní) požadavky}
\paragraph{Uživatelská rozhraní} Máme v plánu vytvořit jednoduché uživatelské rozhraní, ve kterém bude mít uživatel možnost zobrazit nahraný obrázek a tento nahrát do webové služby k přečtení. Přečtená data budou zobrazena ve formě JSON (viz příloha \ref{appendix:prGUI})

\paragraph{Chování za chybových situací}
\paragraph{Požadavky na dokumentaci} Dokumentace bude dostupná na webových stránkách produktu a bude rozdělena na dokumentaci služby a aplikace samotné.
\paragraph{Předávací podmínky} Formální proces předání v tomto případě neexistuje -- aplikace je založena na placení licenčních poplatků za přečtené faktury. Tímto je aplikace dobře škálovatelná pro zákazníky po celém světě.
\begin{appendices}
    \section{GUI}\label{appendix:prGUI}
\begin{figure}[H]
    \centering  
    \includegraphics[width=\textwidth]{pics/guiDone.png}
    \caption{Navrhované GUI je plně postačující pro naše účely.}
    \label{}
\end{figure}
\end{appendices}


\bibliographystyle{abbrv}
\bibliography{main}

\end{document}